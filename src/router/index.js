import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

// export default new Router({
//   routes: [
//     {
//       path: '/',
//       name: 'HelloWorld',
//       component: HelloWorld
//     }
//   ]
// })

const routes = [
  {
    path: '/',
    component: () => import('@/views/bpmn/index.vue')
  },
  {
    path: '/basic',
    component: () => import('@/views/bpmn/basic.vue')
  },
  {
    path: '/panel',
    component: () => import('@/views/bpmn/panel.vue')
  },
  {
    path: '/axios',
    component: () => import('@/views/bpmn/axios.vue')
  },
  {
    path: '/save',
    component: () => import('@/views/bpmn/save.vue')
  },
  {
    path: '/download',
    component: () => import('@/views/bpmn/download.vue')
  },
  {
    path: '/chinese',
    component: () => import('@/views/bpmn/chinese.vue')
  },
  {
    path: '/event',
    component: () => import('@/views/bpmn/event.vue')
  },
]

const router = new Router({
  routes
})

export default router
